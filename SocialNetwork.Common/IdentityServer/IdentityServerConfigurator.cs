﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace SocialNetwork.Common.IdentityServer
{
    public static class IdentityServerConfigurator
    {
        /// <summary>
        /// Adding verification of tokens on the identity server
        /// </summary>
        /// <param name="identityServerUrl">Identity server address</param>
        public static IServiceCollection AddIdentityServer(this IServiceCollection services, string identityServerUrl)
        {

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
            {
                options.Authority = identityServerUrl;

                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = false
                };
            });

            return services;
        }

        /// <summary>
        /// Configuring the CORS policy for clients
        /// </summary>
        /// <param name="clientsUrl">Addresses of clients who work with the API</param>
        public static WebApplication UseCorsForClients(this WebApplication app, params string[] clientsUrl)
        {
            app.UseCors(builder => builder
            .WithOrigins(clientsUrl)
            .AllowAnyHeader()
            .AllowAnyMethod()
            .AllowCredentials()
            );

            return app;
        }
    }
}
