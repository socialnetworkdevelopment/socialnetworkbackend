﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using SocialNetwork.Common.Settings;
using Utils;

namespace SocialNetwork.Common.Logging
{
    /// <summary>
    /// Provide methods for configuring logging.
    /// </summary>
    public static class LogConfigurator
    {
        /// <summary>
        /// Configure logging to Seq.
        /// </summary>
        public static void ConfigureSerilogToSeq(IHostBuilder hostBuilder, IConfiguration configuration)
        {
            SeqSettings seqSettings = Check.NotNull(configuration.GetSection(nameof(SeqSettings)).Get<SeqSettings>());
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration) // connect serilog to our configuration folder
                .Enrich.FromLogContext() //Adds more information to our logs from built in Serilog 
                .Enrich.WithProperty("Application", 
                    Check.NotNull(configuration
                        .GetSection(nameof(ServiceSettings))
                        .Get<ServiceSettings>())
                    .ServiceName)
                .WriteTo.Console(theme: AnsiConsoleTheme.Code) // decide where the logs are going to be shown
                .WriteTo.Seq(Check.NotEmpty(seqSettings.ServerUrl), restrictedToMinimumLevel: seqSettings.MinimumLevel, apiKey: seqSettings.ApiKey)
                .CreateLogger(); //initialise the logger

            hostBuilder.UseSerilog();
        }
    }
}
