﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

namespace Utils
{
    /// <summary>
    /// Содержит методы проверки аргументов.
    /// </summary>
    public static class Check
    {
        /// <summary>
        /// Проверяет, является ли значение отличным от <see langword="null"/>. Генерирует исключение, если проверяемое значение равно <see langword="null"/>.
        /// </summary>
        /// <typeparam name="T">Тип значения.</typeparam>
        /// <param name="value">Значение.</param>
        /// <param name="valueTitle">Заголовок значения.</param>
        /// <returns>Возвращает значение, успешно прошедшее проверку.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        [return: NotNull]
        public static T NotNull<T>([AllowNull] T value, [CallerArgumentExpression("value")] string valueTitle = "")
        {
            if (string.IsNullOrWhiteSpace(valueTitle))
                throw new ArgumentNullException(nameof(valueTitle));

            if (value == null)
                throw new InvalidOperationException($"The {valueTitle.DoubleQuotes()} value cannot be null.");
            return value;
        }

        /// <summary>
        /// Проверяет, является ли значение отличным от пустой строки или строки, состоящей из пустых символов. Генерирует исключение, если проверяемое значение является пустой строкой или строкой, состоящей из пустых символов.
        /// </summary>
        /// <param name="value">Значение.</param>
        /// <param name="valueTitle">Заголовок значения.</param>
        /// <returns>Возвращает значение, успешно прошедшее проверку.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        [return: NotNull]
        public static string NotEmpty(string? value, [CallerArgumentExpression("value")] string valueTitle = "")
        {
            if (string.IsNullOrWhiteSpace(valueTitle))
                throw new ArgumentNullException(nameof(valueTitle));

            if (string.IsNullOrWhiteSpace(value))
                throw new InvalidOperationException($"The {valueTitle.DoubleQuotes()} value cannot be empty.");
            return value;
        }

        /// <summary>
        /// Проверяет, является ли значение отличным от <see cref="Guid.Empty"/>. Генерирует исключение, если проверяемое значение равно <see cref="Guid.Empty"/>.
        /// </summary>
        /// <param name="value">Значение.</param>
        /// <param name="valueTitle">Заголовок значения.</param>
        /// <returns>Возвращает значение, успешно прошедшее проверку.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        [return: NotNull]
        public static Guid NotEmpty(Guid value, [CallerArgumentExpression("value")] string valueTitle = "")
        {
            if (string.IsNullOrWhiteSpace(valueTitle))
                throw new ArgumentNullException(nameof(valueTitle));

            if (value == Guid.Empty)
                throw new InvalidOperationException($"The {valueTitle.DoubleQuotes()} value cannot be empty.");
            return value;
        }

        /// <summary>
        /// Проверяет, является ли значение положительным числом. Генерирует исключение, если значение не является положительным числом.
        /// </summary>
        /// <param name="value">Значение.</param>
        /// <param name="valueTitle">Заголовок значения.</param>
        /// <returns>Возвращает значение, успешно прошедшее проверку.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public static int IsPositive(int value, [CallerArgumentExpression("value")] string valueTitle = "")
        {
            if (string.IsNullOrWhiteSpace(valueTitle))
                throw new ArgumentNullException(nameof(valueTitle));

            if (value <= 0)
                throw new InvalidOperationException($"The {valueTitle.DoubleQuotes()} value must be greater than zero.");
            return value;
        }

        /// <summary>
        /// Проверяет, является ли значение положительным числом или равным 0. Генерирует исключение, если значение меньше, чем 0.
        /// </summary>
        /// <param name="value">Значение.</param>
        /// <param name="valueTitle">Заголовок значения.</param>
        /// <returns>Возвращает значение, успешно прошедшее проверку.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public static int IsPositiveOrZero(int value, [CallerArgumentExpression("value")] string valueTitle = "")
        {
            if (string.IsNullOrWhiteSpace(valueTitle))
                throw new ArgumentNullException(nameof(valueTitle));

            if (value < 0)
                throw new InvalidOperationException($"The {valueTitle.DoubleQuotes()} value must be greater or equal to zero.");
            return value;
        }
    }
}
