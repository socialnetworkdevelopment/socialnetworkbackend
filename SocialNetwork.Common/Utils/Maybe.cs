﻿namespace Utils
{
    /// <summary>
    /// Static factory methods for <see cref="Maybe{T}"/> types.
    /// </summary>
    public static class Maybe
    {
        /// <summary>
        /// Creates a wrapper for an exception.
        /// </summary>
        /// <param name="exception">The exception to wrap.</param>
        public static Maybe<T> FromException<T>(ValueMissingException exception) where T : notnull => Maybe<T>.FromException(exception);

        /// <summary>
        /// Creates a wrapper for a value.
        /// </summary>
        /// <param name="value">The value to wrap.</param>
        public static Maybe<T> FromValue<T>(T value) where T : notnull => Maybe<T>.FromValue(value);
    }
}
