﻿using System.Globalization;

namespace Utils
{
    /// <summary>
    /// Расширения текстового формтирования.
    /// </summary>
    public static class FormattingExtensions
    {
        /// <summary>
        /// Заключает строку <paramref name="str"/> в двойные кавычки.
        /// </summary>
        /// <param name="str">Строка.</param>
        public static string DoubleQuotes(this string? str) => $"\"{str}\"";

        /// <summary>
        /// Заключает тектсовое значение <paramref name="intValue"/> в двойные кавычки.
        /// </summary>
        /// <param name="intValue">Целочисленное значение.</param>
        public static string DoubleQuotes(this int intValue) => $"\"{intValue.ToString(CultureInfo.InvariantCulture)}\"";

        /// <summary>
        /// Заключает строку <paramref name="str"/> в одинарные кавычки.
        /// </summary>
        /// <param name="str">Строка.</param>
        public static string SingleQuotes(this string? str) => $"'{str}'";

        /// <summary>
        /// Заключает строку <paramref name="str"/> в квадратные скобки.
        /// </summary>
        /// <param name="str">Строка.</param>
        public static string SquareBrackets(this string? str) => $"[{str}]";

        /// <summary>
        /// Заключает строку <paramref name="str"/> в круглые скобки.
        /// </summary>
        /// <param name="str">Строка.</param>
        public static string RoundBrackets(this string? str) => $"({str})";

        /// <summary>
        /// Соединяет строки перечисления строк при помощи заданного разделителя.
        /// </summary>
        /// <param name="values">Перечисление строк.</param>
        /// <param name="separator">Разделитель, при помощи которого соединяются строки.</param>
        public static string JoinStrings(this IEnumerable<string> values, string separator)
        {
            Check.NotNull(values);
            return string.Join(separator, values);
        }

        /// <summary>
        /// Соединяет строки перечисления строк при помощи заданного разделителя.
        /// </summary>
        /// <param name="values">Перечисление строк.</param>
        /// <param name="separator">Разделитель, при помощи которого соединяются строки.</param>
        public static string JoinStrings(this IEnumerable<string> values, char separator)
        {
            Check.NotNull(values);
            return string.Join(separator, values);
        }

        /// <summary>
        /// Заключает строку предположительного значения <paramref name="str"/> в двойные кавычки.
        /// </summary>
        /// <param name="str">Строка.</param>
        public static string DoubleQuotes(this Maybe<string>? str) => $"\"{str?.Value}\"";

        /// <summary>
        /// Заключает строку предположительного значения <paramref name="str"/> в одинарные кавычки.
        /// </summary>
        /// <param name="str">Строка.</param>
        public static string SingleQuotes(this Maybe<string>? str) => $"'{str?.Value}'";

        /// <summary>
        /// Заключает строку предположительного значения <paramref name="str"/> в квадратные скобки.
        /// </summary>
        /// <param name="str">Строка.</param>
        public static string SquareBrackets(this Maybe<string>? str) => $"[{str?.Value}]";

        /// <summary>
        /// Заключает строку предположительного значения <paramref name="str"/> в круглые скобки.
        /// </summary>
        /// <param name="str">Строка.</param>
        public static string RoundBrackets(this Maybe<string>? str) => $"({str?.Value})";

        /// <summary>
        /// Соединяет строки перечисления предположительных значений строк при помощи заданного разделителя.
        /// </summary>
        /// <param name="values">Перечисление строк.</param>
        /// <param name="separator">Разделитель, при помощи которого соединяются строки.</param>
        public static string JoinStrings(this IEnumerable<Maybe<string>> values, string separator)
        {
            Check.NotNull(values);
            return string.Join(separator, values.Select(x => x.Value));
        }
    }
}
