﻿using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Utils
{
    /// <summary>
    /// Представляет расширения регистрации сервисов.
    /// </summary>
    public static class ServiceRegistratorExtensions
    {
        private static IServiceCollection AddByRegistrators(this IServiceCollection services, Assembly assembly)
        {
            ArgumentNullException.ThrowIfNull(services);
            ArgumentNullException.ThrowIfNull(assembly);

            List<Type> registratorTypes = null;
            Type registratorInterfaceType = typeof(IServiceRegistrator);
            try
            {
                registratorTypes = assembly.GetTypes().FilterTypes(registratorInterfaceType);
            }
            catch (ReflectionTypeLoadException loadException)
            {
                registratorTypes = loadException?.Types!.FilterTypes(registratorInterfaceType);
            }
            catch { }

            if (registratorTypes == null)
                return services;

            foreach (Type registratorType in registratorTypes)
            {
                ConstructorInfo registratorCtor = GetParameterlessContructor(registratorType);
                IServiceRegistrator registrator = (IServiceRegistrator)registratorCtor.Invoke(null);
                if (registrator == null)
                    throw new InvalidOperationException($"Failed to create a service registrator of the {registratorType.FullName} type.");
                registrator.AddServices(services);
            }

            return services;
        }

        /// <summary>
        /// Регистрирует коллекцию сервисов заданных сборок <paramref name="assemblies"/> при помощи классов-регистраторов, реализующих интерфейс <see cref="IServiceRegistrator"/>, расположенных в данных сборках.
        /// </summary>
        /// <param name="services">Коллеция сервисов.</param>
        /// <param name="assemblies">Коллекция сборок, для которых выполняется регистрация.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static IServiceCollection AddByRegistrators(this IServiceCollection services, params Assembly[] assemblies)
        {
            ArgumentNullException.ThrowIfNull(assemblies);
            foreach (Assembly assembly in assemblies)
                services.AddByRegistrators(assembly);

            return services;
        }

        /// <summary>
        /// Регистрирует коллекцию сервисов при помощи классов-регистраторов, реализующих интерфейс <see cref="IServiceRegistrator"/>, расположенных сборках текущей исполняемой папки, в которых содержится класс с названием AssemblyMarker.
        /// </summary>
        /// <param name="services">Коллеция сервисов.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static IServiceCollection AddByRegistrators(this IServiceCollection services)
        {
            IReadOnlyList<Assembly> allMarkedAssemblies = MarkedAssemblyProvider.Instance.GetAllMarkedAssemblies("AssemblyMarker");
            return AddByRegistrators(services, allMarkedAssemblies.ToArray());
        }


        private static List<Type> FilterTypes(this IEnumerable<Type> sourceTypes, Type filterInterfaceType)
        {
            ArgumentNullException.ThrowIfNull(sourceTypes);
            ArgumentNullException.ThrowIfNull(filterInterfaceType);
            return sourceTypes.Where(sourceType =>
                sourceType != null &&
                sourceType.IsClass &&
                !sourceType.IsAbstract &&
                filterInterfaceType.IsAssignableFrom(sourceType)).ToList();
        }


        private static ConstructorInfo GetParameterlessContructor(Type instanceType)
        {
            ArgumentNullException.ThrowIfNull(instanceType);
            ConstructorInfo constructorInfo = instanceType.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, Type.EmptyTypes, null);
            if (constructorInfo == null)
                throw new InvalidOperationException($"Failed to get a parameterless constructor of the {instanceType.FullName} type.");

            return constructorInfo;
        }


        /// <summary>
        /// Регистрирует сервис <typeparamref name="TService"/>, имплеметируемый ранее зарегистрированным сервисом <typeparamref name="TRegisteredService"/>.
        /// </summary>
        /// <typeparam name="TService">Тип регистрируемого сервиса.</typeparam>
        /// <typeparam name="TRegisteredService">Тип ранее зарегистрированного сервиса, имплеметирующего тип <typeparamref name="TService"/>.</typeparam>
        /// <param name="services">Возвращает коллекцию зарегистрированных сервисов.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public static IServiceCollection AddAsRegistered<TService, TRegisteredService>(this IServiceCollection services)
            where TService : class
            where TRegisteredService : class, TService
        {
            ArgumentNullException.ThrowIfNull(services);
            Type registeredServiceType = typeof(TRegisteredService);
            ServiceDescriptor registeredDescriptor = services.FirstOrDefault(descriptor => registeredServiceType == descriptor.ServiceType);
            if (registeredDescriptor == null)
                throw new InvalidOperationException($"Failed to get the {registeredServiceType.FullName} service descriptor.");
            services.Add(new ServiceDescriptor(typeof(TService), serviceProvider => serviceProvider.GetRequiredService<TRegisteredService>(), registeredDescriptor.Lifetime));
            return services;
        }
    }
}
