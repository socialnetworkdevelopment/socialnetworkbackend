﻿using System.Collections.Concurrent;
using System.Reflection;

namespace Utils
{
    /// <summary>
    /// Представляет поставщик маркированных сборок.
    /// </summary>
    public sealed class MarkedAssemblyProvider
    {
        private MarkedAssemblyProvider()
        {
        }

        /// <summary>
        /// Экземпляр поставщика маркированных сборок.
        /// </summary>
        public static readonly MarkedAssemblyProvider Instance = new();

        private readonly ConcurrentDictionary<Unit, IReadOnlyList<Assembly>> _Cache = new();
        public IReadOnlyList<Assembly> GetAllMarkedAssemblies(string markerTypeName)
        {
            if (_Cache.TryGetValue(Unit.Value, out IReadOnlyList<Assembly> assemblies))
                return assemblies;

            var rootAssembly = Assembly.GetEntryAssembly();

            var allMarkedAssemblies = GetAllReferencedMarkedAssemblies(rootAssembly, markerTypeName);

            return _Cache.GetOrAdd(Unit.Value, allMarkedAssemblies);
        }

        private static IReadOnlyList<Assembly> GetAllReferencedMarkedAssemblies(Assembly rootAssembly, string markerTypeName)
        {
            var markedAssemblies = new HashSet<Assembly>();
            var visited = new HashSet<string>();
            var queue = new Queue<Assembly>();

            queue.Enqueue(rootAssembly);

            while (queue.Count > 0)
            {
                var assembly = queue.Dequeue();
                visited.Add(assembly.FullName);
                markedAssemblies.Add(assembly);

                var references = assembly.GetReferencedAssemblies();
                foreach (var reference in references)
                {
                    if (visited.Contains(reference.FullName))
                        continue;

                    var loadedReference = SafeLoadAssembly(reference);
                    if (loadedReference != null)
                    {
                        Type assemblyMarker = loadedReference.GetType($"{reference.Name}.{markerTypeName}");
                        if (assemblyMarker != null)
                            queue.Enqueue(loadedReference);
                    }
                }
            }

            return markedAssemblies.ToArray();
        }

        private static Assembly SafeLoadAssembly(AssemblyName assemblyName)
        {
            try
            {
                return Assembly.Load(assemblyName);
            }
            catch
            {
                return null;
            }
        }
    }
}
