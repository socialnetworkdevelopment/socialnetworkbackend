﻿using Microsoft.Extensions.DependencyInjection;

namespace Utils
{
    /// <summary>
    /// Представляет интерфейс регистрации сервисов.
    /// </summary>
    public interface IServiceRegistrator
    {
        /// <summary>
        /// Регистрирует сервисы в коллекцию сервисов.
        /// </summary>
        /// <param name="services">Коллекция сервисов.</param>
        void AddServices(IServiceCollection services);
    }
}
