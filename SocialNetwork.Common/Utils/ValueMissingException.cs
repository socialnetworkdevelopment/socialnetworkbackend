﻿using System.Runtime.Serialization;

namespace Utils
{
    /// <summary>
    /// Представляет исключение генерируемое при отсутствии запрашиваемого значения.
    /// </summary>
    public abstract class ValueMissingException : Exception
    {
        /// <summary>
        /// Создаёт экземпляр типа <see cref="ValueMissingException"/>.
        /// </summary>
        public ValueMissingException() : base(null)
        {
        }

        /// <summary>
        /// Создаёт экземпляр типа <see cref="ValueMissingException"/>.
        /// </summary>
        /// <param name="innerException"><inheritdoc/></param>
        public ValueMissingException(Exception? innerException) : base(null, innerException)
        {
        }

        /// <summary>
        /// Создаёт экземпляр наследника типа <see cref="ValueMissingException"/>.
        /// </summary>
        /// <param name="info"><inheritdoc/></param>
        /// <param name="context"><inheritdoc/></param>
        protected ValueMissingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        /// <summary>
        /// Создаёт и возвращает сообщение об ошибке.
        /// </summary>
        protected abstract string CreateMessage();

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public override string Message
        {
            get
            {
                string message = this.CreateMessage();
                if (string.IsNullOrEmpty(message))
                    throw new InvalidOperationException($"Failed to get the message of the {this.GetType().FullName.DoubleQuotes()} type exception.");
                return message;
            }
        }
    }
}
