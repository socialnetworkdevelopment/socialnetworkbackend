﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.ExceptionServices;

namespace Utils
{
    /// <summary>
    /// A wrapper for either a value missing exception or a value.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    public sealed class Maybe<T>
        where T : notnull
    {
        private readonly ValueMissingException? _Exception;
        [AllowNull]
        private readonly T _Value = default;

        private Maybe(ValueMissingException? exception, [AllowNull] T value, bool isException)
        {
            if (isException)
                _Exception = Check.NotNull(exception);
            else
                _Value = Check.NotNull(value);
        }

        /// <summary>
        /// Creates a wrapper for an exception.
        /// </summary>
        /// <param name="exception">The exception to wrap.</param>
        internal static Maybe<T> FromException(ValueMissingException exception) => new(exception, default, true);

        /// <summary>
        /// Creates a wrapper for a value.
        /// </summary>
        /// <param name="value">The value to wrap.</param>
        internal static Maybe<T> FromValue(T value) => new(default, value, false);

        /// <summary>
        /// Whether this instance is an exception.
        /// </summary>
        public bool IsException => _Exception != null;

        /// <summary>
        /// Whether this instance is a value.
        /// </summary>
        public bool IsValue => !this.IsException;

        /// <summary>
        /// Gets the wrapped exception, if any. Returns <c>null</c> if this instance is not an exception.
        /// </summary>
        public ValueMissingException Exception => this.IsException ?
            _Exception! :
            throw new InvalidOperationException($"There is an evaluated value for this instance of the {this.GetType().FullName.DoubleQuotes()} type.");

        /// <summary>
        /// Gets the wrapped value. If this instance is an exception, then that exception is (re)thrown.
        /// </summary>
        [NotNull]
        public T Value => this.IsException ? throw Rethrow() : _Value!;

        /// <summary>
        /// Gets the wrapped value ot throws an exception.
        /// </summary>
        /// <param name="tryValue">Try value.</param>
        public static implicit operator T(Maybe<T> tryValue) => tryValue.Value;

        /// <summary>
        /// Gets the wrapped value or the <typeparamref name="T"/> default value.
        /// </summary>
        /// <param name="defaultValue">Default value.</param>
        public T? ValueOrDefault(T? defaultValue = default) =>
            this.IsValue ? this.Value : defaultValue;

        /// <summary>
        /// A string representation, useful for debugging.
        /// </summary>
        public override string? ToString() => this.IsException ? this.Exception.Message : this.Value.ToString();

        private Exception Rethrow()
        {
            ExceptionDispatchInfo.Capture(_Exception!).Throw();
            return _Exception!;
        }
    }
}