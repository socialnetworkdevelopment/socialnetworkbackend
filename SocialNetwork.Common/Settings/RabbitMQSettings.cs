﻿namespace SocialNetwork.Common.Settings
{
    public class RabbitMQSettings
    {
        public required string Host { get; init; }

        public ushort Port { get; init; }

        public required string VirtualHost { get; init; }

        public required string UserName { get; init; }

        public required string Password { get; init; }
    }
}
