﻿using Serilog.Events;

namespace SocialNetwork.Common.Settings
{
    public class SeqSettings
    {
        public required string ServerUrl { get; init; }

        public string? ApiKey { get; init; }

        public LogEventLevel MinimumLevel { get; init; }
    }
}
