﻿using MassTransit;
using Microsoft.Extensions.Logging;

namespace SocialNetwork.Common.MassTransit
{
    /// <summary>
    /// MassTransit logger filter. Log correlation id and common proccessing info.
    /// </summary>
    public class MessageLoggerFilter<T> : IFilter<ConsumeContext<T>>
        where T : class
    {
        private readonly ILogger<MessageLoggerFilter<T>> _logger;

        public MessageLoggerFilter(ILogger<MessageLoggerFilter<T>> logger)
        {
            _logger = logger;
        }

        public async Task Send(ConsumeContext<T> context, IPipe<ConsumeContext<T>> next)
        {
            Guid messageGuid = context.MessageId ?? Guid.NewGuid();

            using (_logger.BeginScope("Global request id:{@CorrelationId}", context.CorrelationId.ToString()))
            using (_logger.BeginScope("MassTransit:{@MessageGuid}", messageGuid))
            {
                string requestName = typeof(T).Name;
                
                _logger.LogInformation("[START] {@RequestName}", requestName);

                try
                {
                    try
                    {
                        _logger.LogInformation("[PROPS] {@RequestName} {@Message}", requestName, context.Message);
                    }
                    catch (NotSupportedException)
                    {
                        _logger.LogInformation("[Serialization ERROR] {@RequestName} Could not serialize the request.", requestName);
                    }

                    await next.Send(context);
                }
                finally
                {
                    _logger.LogInformation(
                        "[END] {@RequestName}", requestName);
                }
            }
        }

        public void Probe(ProbeContext context) { }
    }
}
