﻿using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SocialNetwork.Common.Settings;
using System.Reflection;
using Utils;

namespace SocialNetwork.Common.MassTransit
{
    public static class Extensions
    {
        /// <summary>
        /// Register MassTransit with using RabbitMQ.
        /// </summary>
        /// <param name="assemblies">Assemblies with consumers.</param>
        public static IServiceCollection AddMassTransitWithRabbitMq(this IServiceCollection services, params Assembly[] assemblies)
        {
            services.AddMassTransit(configure =>
            {
                configure.AddConsumers(assemblies);

                configure.UsingRabbitMq((context, configurator) =>
                {
                    IConfiguration configuration = context.GetRequiredService<IConfiguration>();
                    ServiceSettings serviceSettings = 
                        Check.NotNull(configuration
                            .GetSection(nameof(ServiceSettings))
                            .Get<ServiceSettings>());
                    RabbitMQSettings rabbitMQSettings = 
                        Check.NotNull(configuration
                            .GetSection(nameof(RabbitMQSettings))
                            .Get<RabbitMQSettings>());
                    configurator.Host(rabbitMQSettings.Host, rabbitMQSettings.Port, rabbitMQSettings.VirtualHost, 
                        hostConfigurator => 
                        {
                            hostConfigurator.Username(rabbitMQSettings.UserName);
                            hostConfigurator.Password(rabbitMQSettings.Password);
                        });

                    // Correlation Id
                    configurator.UseConsumeFilter(typeof(SetCorrelationIdToHttpContextFilter<>), context);
                    configurator.UseConsumeFilter(typeof(MessageLoggerFilter<>), context);
                    configurator.UsePublishFilter(typeof(SetCorrelationIdFilter<>), context);

                    configurator.ConfigureEndpoints(context, new KebabCaseEndpointNameFormatter(serviceSettings.ServiceName, false));
                        configurator.UseMessageRetry(retryConfigurator =>
                        {
                            retryConfigurator.Interval(3, TimeSpan.FromSeconds(5));
                        });
                });
            });

            return services;
        }
    }

}
