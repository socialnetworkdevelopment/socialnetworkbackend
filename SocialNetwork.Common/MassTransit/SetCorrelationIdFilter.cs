﻿using MassTransit;
using SocialNetwork.Common.CorrelationId;

namespace SocialNetwork.Common.MassTransit
{
    /// <summary>
    /// MassTransit filter for setting correlation id in publishing context.
    /// </summary>
    public class SetCorrelationIdFilter<T> : IFilter<PublishContext<T>>
        where T : class
    {
        private readonly ICorrelationIdAccessor _correlationIdAccessor;

        public SetCorrelationIdFilter(ICorrelationIdAccessor correlationIdAccessor)
        {
            _correlationIdAccessor = correlationIdAccessor;
        }

        public async Task Send(PublishContext<T> context, IPipe<PublishContext<T>> next)
        {
            Guid correlationId = _correlationIdAccessor.GetCorrelationId();
            if (correlationId == default)
                context.CorrelationId = NewId.NextGuid();
            else
                context.CorrelationId = correlationId;

            await next.Send(context);
        }

        public void Probe(ProbeContext context) { }
    }
}
