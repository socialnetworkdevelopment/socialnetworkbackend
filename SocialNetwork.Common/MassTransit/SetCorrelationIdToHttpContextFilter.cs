﻿using MassTransit;
using SocialNetwork.Common.CorrelationId;

namespace SocialNetwork.Common.MassTransit
{
    public class SetCorrelationIdToHttpContextFilter<T> : IFilter<ConsumeContext<T>>
        where T : class
    {
        public SetCorrelationIdToHttpContextFilter()
        {
        }

        public async Task Send(ConsumeContext<T> context, IPipe<ConsumeContext<T>> next)
        {
            using (CorrelationIdContext.BeginScope(context.CorrelationId))
            {
                await next.Send(context);
            }
        }

        public void Probe(ProbeContext context) { }
    }
}
