﻿using Microsoft.AspNetCore.Http;

namespace SocialNetwork.Common.CorrelationId
{
    /// <summary>
    /// Provide access to correlation id.
    /// </summary>
    public interface ICorrelationIdAccessor
    {
        /// <summary>
        /// Provide correlation id.
        /// </summary>
        Guid GetCorrelationId();
    }

    internal class CorrelationIdAccessor : ICorrelationIdAccessor
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CorrelationIdAccessor(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public Guid GetCorrelationId()
        {
            Guid correlationId = Guid.Empty;
            if (_httpContextAccessor.HttpContext != null)
            {
                object correlationIdObj = _httpContextAccessor.HttpContext.Items[Consts.CorrelationId.CorrelationIdHeader];
                if (correlationIdObj != null)
                    correlationId = Guid.Parse((string)correlationIdObj);
            }
            else if (CorrelationIdContext.HasCurrent())
            {
                correlationId = CorrelationIdContext.GetCurrent().CorrelationId ?? Guid.Empty;
            }

            return correlationId;
        }
    }
}
