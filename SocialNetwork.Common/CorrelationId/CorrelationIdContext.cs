﻿namespace SocialNetwork.Common.CorrelationId
{
    /// <summary>
    /// Context for correlation id.
    /// </summary>
    public class CorrelationIdContext : IDisposable
    {
        [ThreadStatic]
        private static CorrelationIdContext? _current;

        private CorrelationIdContext(Guid? correlationId)
        {
            CorrelationId = correlationId;
        }

        public Guid? CorrelationId { get; set; }

        public void Dispose()
        {
            _current = null;
        }

        public static CorrelationIdContext? GetCurrent()
            => _current;

        public static bool HasCurrent()
            => _current != null;

        public static IDisposable BeginScope(Guid? correlationId)
        {
            if (_current == null)
                _current = new CorrelationIdContext(correlationId);

            return _current;
        }
    }
}
