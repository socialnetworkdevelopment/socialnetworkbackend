﻿namespace SocialNetwork.Common.CorrelationId
{
    /// <summary>
    /// Handler for outgoing requests to set correlation id in headers.
    /// </summary>
    public class CorrelationIdRequestHandler : DelegatingHandler
    {
        private readonly ICorrelationIdAccessor _correlationIdAccessor;

        public CorrelationIdRequestHandler(ICorrelationIdAccessor correlationIdAccessor)
        {
            _correlationIdAccessor = correlationIdAccessor;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            request.Headers.Add(Consts.CorrelationId.CorrelationIdHeader, _correlationIdAccessor.GetCorrelationId().ToString());
            return base.SendAsync(request, cancellationToken);
        }
    }
}
