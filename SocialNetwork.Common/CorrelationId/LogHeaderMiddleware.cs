﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace SocialNetwork.Common.CorrelationId
{
    /// <summary>
    /// Middleware for logging request's correlation id.
    /// </summary>
    internal class LogHeaderMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ICorrelationIdAccessor _correlationIdAccessor;

        public LogHeaderMiddleware(RequestDelegate next, ICorrelationIdAccessor correlationIdAccessor)
        {
            _next = next;
            _correlationIdAccessor = correlationIdAccessor;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            Guid correlationId = _correlationIdAccessor.GetCorrelationId();
            if (correlationId == default)
            {
                await _next(context);
            }
            else
            {
                ILogger<LogHeaderMiddleware> logger = context.RequestServices.GetRequiredService<ILogger<LogHeaderMiddleware>>();
                using (logger.BeginScope("Global request id:{@CorrelationId}", correlationId))
                {
                    await _next(context);
                }
            }
        }
    }
}
