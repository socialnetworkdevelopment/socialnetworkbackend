﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace SocialNetwork.Common.CorrelationId
{
    public static class Extensions
    {
        /// <summary>
        /// Add middleware for setting and logging correlation id.
        /// </summary>
        public static IApplicationBuilder UseCorrelationIdMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CorrelationIdHeaderMiddleware>()
                .UseMiddleware<LogHeaderMiddleware>();
        }

        /// <summary>
        /// Add request handler for setting correlation id.
        /// </summary>
        public static IHttpClientBuilder AddCorrelationIdRequestHandler(this IHttpClientBuilder httpClientBuilder)
        {
            return httpClientBuilder.AddHttpMessageHandler<CorrelationIdRequestHandler>();
        }

        /// <summary>
        /// Register additional services for correlation id.
        /// </summary>
        public static IServiceCollection AddCorrelationIdAdditionalServices(this IServiceCollection serviceCollection)
        {
            return serviceCollection
                .AddHttpContextAccessor()
                .AddTransient<ICorrelationIdAccessor, CorrelationIdAccessor>();
        }
    }
}
