﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace SocialNetwork.Common.CorrelationId
{
    /// <summary>
    /// Middleware for setting correlation id in http context.
    /// </summary>
    public class CorrelationIdHeaderMiddleware
    {
        private readonly RequestDelegate _next;

        public CorrelationIdHeaderMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            StringValues header = context.Request.Headers[Consts.CorrelationId.CorrelationIdHeader];
            string sessionId;

            if (header.Count > 0)
            {
                sessionId = header[0]!;
            }
            else
            {
                sessionId = Guid.NewGuid().ToString();
            }

            context.Items[Consts.CorrelationId.CorrelationIdHeader] = sessionId;
            await _next(context);
        }
    }
}
