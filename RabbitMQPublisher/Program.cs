﻿using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using SocialNetwork.Contracts.User;
using SocialNetwork.Common.MassTransit;
using System.Reflection;
using SocialNetwork.Common.CorrelationId;

internal class Program
{
    private static async Task Main(string[] args)
    {
        var host = AppStartup();
        
        try
        {
            var publishEndpoint = host.Services.GetService<IPublishEndpoint>();
            await publishEndpoint.Publish(new UserCreated(7,"Ivan","Ivanov",30));
            Log.Information("Сообщение отправлено.");
            Console.ReadLine();
        }
        catch (Exception ex)
        {
            Log.Error(ex.ToString());
        }
    }

    static void BuildConfig(IConfigurationBuilder builder)
    {
        builder.SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddEnvironmentVariables();
    }

    static IHost AppStartup()
    {
        var builder = new ConfigurationBuilder();
        BuildConfig(builder);
        var configRoot = builder.Build();

        Log.Logger = new LoggerConfiguration()
                        .ReadFrom.Configuration(configRoot)
                        .Enrich.FromLogContext()
                        .WriteTo.Console()
                        .CreateLogger();

        var host = Host.CreateDefaultBuilder()
                    .ConfigureServices((context, services) => {
                        services.AddCorrelationIdAdditionalServices();
                        services.AddMassTransitWithRabbitMq(Assembly.GetEntryAssembly());
                        
                    })
                    .UseSerilog()
                    .Build();

        return host;
    }
}