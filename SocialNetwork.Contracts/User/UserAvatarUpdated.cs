﻿namespace SocialNetwork.Contracts.User
{
    public record UserAvatarUpdated(long UserId, long AvatarId);
}
