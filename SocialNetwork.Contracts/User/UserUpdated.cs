﻿namespace SocialNetwork.Contracts.User
{
    public record UserUpdated(long UserId, string FirstName, string LastName, int Age);
}
