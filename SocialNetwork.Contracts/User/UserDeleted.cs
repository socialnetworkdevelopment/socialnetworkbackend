﻿namespace SocialNetwork.Contracts.User
{
    public record UserDeleted(long UserId);
}
