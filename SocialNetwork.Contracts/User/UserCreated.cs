﻿namespace SocialNetwork.Contracts.User
{
    public record UserCreated(long UserId, string FirstName, string LastName, long AvatarId, int Age);
}
