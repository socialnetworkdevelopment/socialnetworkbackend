﻿using ChatHubTester;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System.Text;

internal class Program
{
    static string _key = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";

    public static async Task Main(string[] args)
    {
        try
        {
            IConfiguration config = BuildConfig();
            AppStartup();
            ChatSettings settings = config.GetRequiredSection(nameof(ChatSettings)).Get<ChatSettings>()
                ?? throw new Exception("UserSettings not found.");

            var url = "https://localhost:7119/chathub";

            HubConnection connection = new HubConnectionBuilder()
                .WithUrl(url,
                 options =>
                 {
                     options.AccessTokenProvider = () => Task.FromResult(JwtTokenProvider.GenerateToken(settings, _key));
                 })
                .WithAutomaticReconnect()
                .Build();

            connection.On<int, int, string>("ReceiveOne", (receiverId, autorId, message) =>
            {
                Console.WriteLine($"{autorId}: {message}");
            });

            connection.On<string>("SendOne", message =>
            {
                Console.WriteLine($"{settings.SenderId}: {message}");
            });

            await connection.StartAsync();

            Console.WriteLine("Please Enter Message to start chat.");
            while (true)
            {
                string? message = Console.ReadLine();

                if (string.Compare(message, "exit", StringComparison.OrdinalIgnoreCase) == 0)
                    break;

                if (string.IsNullOrEmpty(message))
                {
                    break;
                }

                await connection.InvokeAsync<string>("SendMessage", settings.RecieverId, 1, message).ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        Console.WriteLine("There was an error calling send: {0}",
                                          task.Exception.GetBaseException());
                    }
                });
            }

            await connection.StopAsync();
        }
        catch (Exception ex)
        {
            Console.ReadKey();
        }
    }


    static IConfiguration BuildConfig()
    {
        IConfiguration config = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddEnvironmentVariables()
            .Build();
        return config;
    }

    static IHost AppStartup()
    {
        var host = Host.CreateDefaultBuilder()
                    .ConfigureServices((context, services) =>
                    {
                        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    
                    .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
                    {
                        options.SaveToken = true;
                        options.RequireHttpsMetadata = false;
                        options.IncludeErrorDetails = true;
                        options.TokenValidationParameters = new TokenValidationParameters()
                        {
                            ValidateIssuer = false,
                            ValidateAudience = false,
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_key))
                        };
                        options.Events = new JwtBearerEvents
                        {
                            OnAuthenticationFailed = context =>
                            {
                                if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                                {
                                    context.Response.Headers.Add("Token-Expired", "true");
                                }
                                return Task.CompletedTask;
                            },
                        };
                    });
                    })
                    .Build();

        return host;
    }
}